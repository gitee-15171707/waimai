package com.xyl.mydelivery.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xyl.mydelivery.entity.User;
import com.xyl.mydelivery.mapper.UserMapper;
import com.xyl.mydelivery.service.UserService;
import com.xyl.mydelivery.sms.tencent.TencentSendSmsUtils;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User>
    implements UserService {

    // @Value("${spring.mail.username}")
    // private String from;   // 邮件发送人

    // @Autowired
    // private JavaMailSender mailSender;


    @Override
    public void sendMsg(String to,String subject,String context){
        // SimpleMailMessage mailMessage = new SimpleMailMessage();
        //
        // mailMessage.setFrom(from);
        // mailMessage.setTo(to);
        // mailMessage.setSubject(subject);
        // mailMessage.setText(context);
        //
        // // 真正的发送邮件操作，从 from到 to
        // mailSender.send(mailMessage);

    }

    @Override
    public void sendSms(String toPhone, String templateParam){
        SimpleMailMessage mailMessage = new SimpleMailMessage();

        // mailMessage.setFrom(from);
        mailMessage.setTo(toPhone);
        // mailMessage.setSubject(subject);
        mailMessage.setText(templateParam);

        // 真正的发送短信，从 from到 to
        TencentSendSmsUtils.sendSmsStatic(mailMessage);
    }

}


