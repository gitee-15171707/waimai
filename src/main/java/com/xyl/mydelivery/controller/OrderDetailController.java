package com.xyl.mydelivery.controller;

import com.xyl.mydelivery.common.Result;
import com.xyl.mydelivery.entity.OrderDetail;
import com.xyl.mydelivery.service.OrdersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/orderDetail")
@Slf4j
public class OrderDetailController {

    @Autowired
    private OrdersService ordersService;

    @GetMapping("/{id}")
    public Result<List> queryOrderDetailById(@PathVariable Long id) {

        // Long orderId = item.getId();//获取订单id
        List<OrderDetail> orderDetailList = this.ordersService.getOrderDetailsByOrderId(id);
        List<OrderDetail> collect = orderDetailList.stream().filter(a -> a.getNumber() > 0).collect(Collectors.toList());
        return Result.success(collect);
    }
}
